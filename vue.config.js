const { defineConfig } = require('@vue/cli-service')


module.exports = defineConfig({
  transpileDependencies: true,
  publicPath:"./",
  // css: {
  //   loaderOptions: {
  //     postcss: {
  //       plugins: [
  //         require('postcss-pxtorem')({
  //           rootValue: 16, // 设计稿宽度的1/10，通常是750的1/10
  //           propList: ['*'], // 需要转换的属性，这里选择转换所有属性
  //         }),
  //       ],
  //     },
  //   },
  // },
  devServer: {
    port: 8085,
    proxy: {
      //配置自动启动浏览器
      "/zbzt": {
        // target: "http://192.168.1.14:8082", // 许
        // target: "http://192.168.31.190:8082", // 许
        // target: "http://192.168.31.54:8082",// 星
        // target: "http://192.168.1.13:8082",// 星
        // target: "http://219.139.191.20:4071",
        // target: "http://47.96.154.183:8013",
        // target: "http://124.222.10.204:8053",
        // http://192.168.3.123/healthy-mn-admin/
        target:"https://api.zbzt360.cn/zbzt",//dev环境
        //target:"http://192.168.3.50:12022/HealthyMnHt",//李居帅
        changeOrigin: true,
        secure: false,
        // pathRewrite: {   //路径重写
        //   '^/HealthyMnHt':''          
        // },
      },
    },
    open: true, //运x行打开浏览器
  },
})
