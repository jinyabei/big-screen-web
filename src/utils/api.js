import axios from 'axios'
import { ElMessage } from 'element-ui'

import BASE_URL from './baseurl'

const Request = axios.create({
  baseURL: BASE_URL,
  withCredentials: true,
  timeout: 12000000
})
const onRequest = (req) => {
  return req
}

const onRequestError = (err) => {
  return Promise.reject(err)
}

const onResponse = (res) => {
  if (res.headers.web_is_refresh == '1') {
    window.location.reload()
  }
  if (res.data.status == '401') {
    // ElNotification({
    //   message: res.data.content,
    //   type: 'error',
    //   duration: 2000
    // })
    return
  }
  const code = res.data.code
  switch (code) {
    case 0:
      ElMessage({
        message: res.data.message ? res.data.message : res.message,
        type: 'warning'
      })
      break
    case 500:
      // ElNotification({
      //   message: res.data.message || '出错了',
      //   type: 'error',
      //   duration: 2000
      // })
      break
    case 401:
      // ElNotification({
      //   message: res.data.message,
      //   type: 'error',
      //   duration: 2000
      // })
      break
  }
  return res.data
}
let isNotice = false
const onResponseError = (err) => {
  const title = err.response.data.content || '出错了！'
  let message = ''
  if (err.response) {
    switch (err.response.data.status) {
      case '401':
        message = err.response.data.message
        break
      case 403:
        message = '拒绝执行'
        break
      case 404:
        message = '资源未找到'
        break
      case 405:
        message = '方法错误'
        break
      case 409:
        message = '未登录'
        // out = true
        break
      case 500:
        message = '无法完成请求'
        break
      case 503:
        message = '暂时无法处理请求'
        break
      default:
        message = err.response.data.errmsg
    }
  }
  if (!isNotice) {
    isNotice = true
    console.log(message,title);
    // ElNotification({
    //   title,
    //   message,
    //   type: 'error',
    //   duration: 2000
    // })
  }
  setTimeout(() => {
    isNotice = false
  }, 2000);
  return Promise.reject(err)
}

Request.interceptors.request.use(onRequest, onRequestError)
Request.interceptors.response.use(onResponse, onResponseError)



export default Request
