import Request from '@/utils/api'

// 产业大屏
export const industrial = (params) => Request.get('/zbzt/iscreen/industrial', params)

// 招商大屏
export const invest = (params) => Request.get('zbzt/iscreen/invest', params)

//获取天气
export const getWeather = ()=>Request.get("zbzt/iscreen/weather/%E5%8C%97%E4%BA%AC")
