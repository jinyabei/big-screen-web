const points = [
    {
        value: [118.31, 26.07, 500], //福建
        itemStyle: {
            color: '#FEFF96',
        },
    },
    {
        value: [113.98, 23.52, 500], //广东
        itemStyle: {
            color: '#FEFF96',
        },
    },
    {
        value: [113.46, 34.25, 500], //河南
        itemStyle: {
            color: '#FEFF96',
        },
    },
    {
        value: [112.29, 30.98, 500], //湖北
        itemStyle: {
            color: '#FEFF96',
        },
    },
    {
        value: [112.08, 27.79, 500], //湖南
        itemStyle: {
            color: '#FEFF96',
        },
    },
    {
        value: [120.26, 32.54, 500], //江苏
        itemStyle: {
            color: '#FEFF96',
        },
    },
    {
        value: [123.42, 41.29, 500], //辽宁
        itemStyle: {
            color: '#FEFF96',
        },
    },
    {
        value: [118.01, 36.37, 500], //山东
        itemStyle: {
            color: '#FEFF96',
        },
    },
    {
        value: [101.71, 24.84, 500], //云南
        itemStyle: {
            color: '#FEFF96',
        },
    },
    {
        value: [128.34, 47.05, 500], //黑龙江
        itemStyle: {
            color: '#FEFF96',
        },
    },
    {
        value: [120.15, 29.28, 500], //浙江
        itemStyle: {
            color: '#FEFF96',
        },
    },
    {
        value: [114.05, 22.55, 1000, '深圳'], //深圳
        itemStyle: {
            color: '#FEFF96',
        },
    },
    {
        value: [108.93, 34.27, 1000, '西安'], //西安
        itemStyle: {
            color: '#FEFF96',
        },
    },
    {
        value: [113.9, 35.3, 1000, '新乡'], //新乡
        itemStyle: {
            color: '#FEFF96',
        },
    },
    {
        value: [112.93, 28.23, 1000, '长沙'], //长沙
        itemStyle: {
            color: '#FEFF96',
        },
    },
];

export default points