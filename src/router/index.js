import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    redirect: "/navigation",
  },
  {
    path:'/navigation',
    name:'navigation',
    component:() => import('../views/navigation/index.vue'),
    meta:{
      "title":"招商驾驶舱"
    }
  },
  {
    path:'/industry',
    name:'industry',
    component:() => import('../views/industry/index.vue'),
    meta:{
      "title":"四川省重庆市产业数据大屏"
    }
  },
  {
    path:'/investment',
    name:'investment',
    component:() => import('../views/investment/invest-componet.vue'),
    meta:{
      "title":"中北政通招商数云图"
    }
  }
]
const router = new VueRouter({
  routes
})

router.beforeEach((to, from, next) => {
  if(to.meta&&to.meta.title){
    document.title = to.meta.title
  }
  next()
})

export default router