/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/**/*.{vue,html,js}"],
  theme: {
    extend: {
      height:{
        '4vh': '4vh',
        '18vh': '18vh',
        '19vh': '19vh',
        '20vh': '20vh',
        '21vh': '21vh',
        '22vh': '22vh',
        '23vh': '23vh',
        '30vh': '30vh',
        '36vh': '36vh',
        '40vh': '40vh',
        '50vh': '50vh',
        '54vh': '54vh',
        '80vh': '80vh',
      },
      padding:{
        '2vh': '2vh',
        '4vh': '4vh',
      },
      margin:{
        '2vh': '2vh',
        '4vh': '4vh',
        '6vh': '6vh',
        '8vh': '8vh',
      }
    },
  },
  plugins: [],
}

